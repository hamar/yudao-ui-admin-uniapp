import request from '@/utils/request'

//获取租户id
export function getTenantIdByName (name) {
	return request({
		url: '/admin-api/system/tenant/get-id-by-name?name=' + name,
		headers: {
			isToken: false
		},
	})
}
// 登录方法
export function login(tenantName, username, password, captchaVerification) {
	const data = {
		tenantName,
		username,
		password,
		captchaVerification
	}
	return request({
		url: '/admin-api/system/auth/login',
		headers: {
			isToken: true
		},
		'method': 'POST',
		'data': data
	})
}

// 获取用户详细信息
export function getInfo() {
	return request({
		url: '/admin-api/system/auth/get-permission-info',
		'method': 'GET'
	})
}

// 退出方法
export function logout() {
	return request({
		url: '/admin-api/system/auth/logout',
		'method': 'POST'
	})
}
