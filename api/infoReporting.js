// 信息填报，地块选择
import request from '@/utils/request'

//获取地块
export function getOrChardLangList (params) {
	return request({
		url: '/app-api/orchard/land/list',
		method: 'GET',
    params
	})
}

//获取用户填报字段
export function queryFieldConfigByUser (params) {
	return request({
		url: '/app-api/orchard/orchardData/queryFieldConfigByUser',
		method: 'GET',
    params
	})
}

//用户填报
export function addOrchardDataWrite (data) {
	return request({
		url: '/app-api/orchard/orchardData/addOrchardDataWrite',
		method: 'POST',
    data
	})
}