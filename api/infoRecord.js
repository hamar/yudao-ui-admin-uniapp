// 填报历史记录查询
import request from '@/utils/request'

//填报记录
export function orchardDataWrite (params) {
	return request({
		url: '/app-api/orchard/orchardData/list/orchardDataWrite',
		method: 'GET',
    params
	})
}