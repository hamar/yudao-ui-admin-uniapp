const getters = {
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  roles: state => state.user.roles,
  permissions: state => state.user.permissions,
  tenantId: state => state.user.tenantId,
  landData: state => state.user.landData,
	userId: state => state.user.userId
}
export default getters
