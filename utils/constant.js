const constant = {
   avatar: 'vuex_avatar',
   name: 'vuex_name',
   roles: 'vuex_roles',
   permissions: 'vuex_permissions',
   tenantId: 'vuex_tenantId',
   landData: 'vuex_landData',
	 userId: 'vuex_userId',
 }

 export default constant
